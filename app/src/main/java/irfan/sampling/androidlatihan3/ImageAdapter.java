package irfan.sampling.androidlatihan3;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {

    private Context ctx;

    public Integer[] datagambar ={
      R.drawable.jkt, R.drawable.bdg, R.drawable.sby,
      R.drawable.dpk, R.drawable.bgr
    };

    public ImageAdapter(Context c){
        ctx = c;
    }

    @Override
    public int getCount() {
        return datagambar.length;
    }

    @Override
    public Object getItem(int i) {
        return datagambar[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imgView = new ImageView(ctx);
        imgView.setImageResource(datagambar[i]);
        imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imgView.setLayoutParams(new GridView.LayoutParams(260, 260));
        return imgView;
    }
}
